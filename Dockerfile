FROM centos 
MAINTAINER mwagh1412@gmail.com 

RUN yum install httpd -y
COPY index.html /var/www/html/index.html
ENV HOSTNAME = image.demo
CMD ["/usr/sbin/httpd","-DFORGROUND"] 

